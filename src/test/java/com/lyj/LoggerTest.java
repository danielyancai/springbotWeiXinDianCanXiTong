package com.lyj;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.naming.Name;

/**
 * Created by lyj on 2017/10/12.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class LoggerTest {

    /**
     * 第一种测试方法
     */
    //private  final Logger logger= LoggerFactory.getLogger(LoggerTest.class);

    @Test
    public void  test1(){
        /**
         * 第一种测试方法
         */
       /* logger.debug("debug...");
        logger.info("info...");
        logger.error("error..");*/
        /**
         * 第二种注解测试方法a
         */
        String name="lyj";
        String password="123456";
        log.debug("debug...");
        log.info("name:{},password:{}",name,password);
        log.error("error...");

    }
}
