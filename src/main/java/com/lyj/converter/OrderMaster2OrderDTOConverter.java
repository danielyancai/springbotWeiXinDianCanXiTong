package com.lyj.converter;

import com.lyj.dataobject.OrderMaster;
import com.lyj.dto.OrderDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换器将OrderMaster转换成rderDTO
 * Created by lyj on 2017/10/17.
 */
public class OrderMaster2OrderDTOConverter {

    public static OrderDTO convert(OrderMaster orderMaster){
        OrderDTO orderDTO=new OrderDTO();
        BeanUtils.copyProperties(orderMaster,orderDTO);
        return orderDTO;
    }

    public static List<OrderDTO> convert(List<OrderMaster> orderMasterList){
       return orderMasterList.stream().map(e ->
             convert(e)
        ).collect(Collectors.toList());
    }

}
