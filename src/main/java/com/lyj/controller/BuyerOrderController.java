package com.lyj.controller;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.lyj.converter.OrderForm2OrderDTOConverter;
import com.lyj.dto.OrderDTO;
import com.lyj.enums.ResultEnum;
import com.lyj.exception.SellException;
import com.lyj.form.OrderForm;
import com.lyj.service.BuyerService;
import com.lyj.service.OrderService;
import com.lyj.utils.ResultVoUtil;
import com.lyj.vo.ResultVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lyj on 2017/10/27.
 * 订单controller
 */
@RestController
@RequestMapping("/buyer/oder/")
@Slf4j
public class BuyerOrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private BuyerService buyerService;

    /**
     * 创建订单
     * @param orderForm
     * @param bindingResult
     * @return
     */
    @PostMapping("create")
    public ResultVo<Map<String,String>> create(@Valid OrderForm orderForm,
                                               BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            log.error("【创建订单】参数不正确，orderForm={}",orderForm);
            throw new SellException(ResultEnum.FARAM_ERROR.getCode(),bindingResult.getFieldError().getDefaultMessage());
        }

        OrderDTO orderDTO= OrderForm2OrderDTOConverter.converter(orderForm);
        if (CollectionUtils.isEmpty(orderDTO.getOrderDetailList())){
            log.error("【创建订单】购物车不忘为空");
            throw new SellException(ResultEnum.CART_EMPTY);
        }
        OrderDTO createResult=orderService.create(orderDTO);
        Map<String,String> map=new HashMap<>();
        map.put("orderId",createResult.getOrderId());

        return ResultVoUtil.success(map);
    }

    /**
     * 订单列表
     * @param openid
     * @param page
     * @param size
     * @return
     */
    @PostMapping("list")
    public ResultVo<OrderDTO> list(@RequestParam("openid") String openid,
                                   @RequestParam(value = "page",defaultValue = "0") Integer page,
                                   @RequestParam(value = "size",defaultValue = "10") Integer size){
        if (StringUtils.isEmpty(openid)){
            log.error("【查询订单列表】openid为空");
            throw new SellException(ResultEnum.FARAM_ERROR);
        }
        PageRequest request=new PageRequest(page,size);
        Page<OrderDTO> orderDTOPage= orderService.findList(openid,request);

        return ResultVoUtil.success(orderDTOPage.getContent());

    }

    /**
     * 订单详情
     * @param openid
     * @param orderId
     * @return
     */
    @PostMapping("detail")
    public ResultVo<OrderDTO> detail(@RequestParam("openid") String openid,
                                     @RequestParam("orderId") String orderId){

        //检查是否是自己的订单
        OrderDTO orderDTO=buyerService.findOrderOne(openid,orderId);

      // OrderDTO orderDTO=orderService.findOne(orderId);
       return ResultVoUtil.success(orderDTO);
    }

    /**
     * 取消订单
     * @param openid
     * @param orderId
     * @return
     */
    @PostMapping("cancel")
    public ResultVo cancel(@RequestParam("openid") String openid,
                           @RequestParam("orderId") String orderId){

        //取消操作
        buyerService.ordercancel(openid,orderId);
//        OrderDTO orderDTO=orderService.findOne(orderId);
//        orderService.cancel(orderDTO);

        return ResultVoUtil.success();

    }

}
