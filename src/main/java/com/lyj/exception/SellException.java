package com.lyj.exception;

import com.lyj.enums.ResultEnum;

/**
 * Created by lyj on 2017/10/16.
 */
public class SellException extends RuntimeException {

    private Integer code;

    public SellException(ResultEnum resultEnum) {
       super(resultEnum.getMessage());
        this.code=resultEnum.getCode();
    }

    public SellException(Integer code,String message) {
        super(message);
        this.code = code;
    }


}
