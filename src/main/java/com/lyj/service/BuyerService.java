package com.lyj.service;

import com.lyj.dto.OrderDTO;

/**
 * 买家
 * Created by lyj on 2017/10/27.
 */
public interface BuyerService {

    //查询一个订单
    OrderDTO findOrderOne(String openid,String orderId);

    //取消订单
    OrderDTO ordercancel(String openid,String orderId);

}
