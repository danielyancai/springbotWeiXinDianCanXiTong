package com.lyj.service;

import com.lyj.dataobject.ProductCategory;

import java.util.List;

/**
 * Created by lyj on 2017/10/13.
 */
public interface CategoryService {

    //根据id查询
    ProductCategory findOne(Integer categoryId);

    //查询所有
    List<ProductCategory> findAll();

    //通过类型查询
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);

    //添加
    ProductCategory save(ProductCategory productCategory);

}
