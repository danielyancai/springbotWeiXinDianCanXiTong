package com.lyj.service;

import com.lyj.dataobject.ProductInfo;
import com.lyj.dto.CartDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * 商品
 * Created by lyj on 2017/10/13.
 */
public interface ProductService {

    /**
     * 根据id查询
     * @param productId
     * @return
     */
    ProductInfo findOne(String productId);

    /**
     * 查询所有在家商品列表
     * @return
     */
    List<ProductInfo> findUpAll();

    /**
     * 分页查询
     * @return
     */
    Page<ProductInfo> findAll(Pageable pageable);

    /**
     * 添加
     * @param productInfo
     * @return
     */
    ProductInfo save(ProductInfo productInfo);

    //加库存
    void increaseStock(List<CartDTO> cartDTOList);

    //减库存
    void decreaseStock(List<CartDTO> cartDTOList);

}
